const API_URL = 'https://ajax.test-danit.com/api/swapi/films'

function sendRequest(url, method, options) {
    return fetch(url, {method, ...options})
        .then(response => response.json())
}

const filmsList = document.querySelector('.films-list');

class FilmCard {
    constructor(id, title, description) {
        this._id = id;
        this._title = title;
        this._description = description;
        this._element = document.createElement('div');
        this._element.classList.add('film__card');

    }

    get element() {
        return this._element;
    }

    render() {
        this._element.innerHTML = `
           <div class="film__card-box">
               <h3>${this._title}: ${this._id}</h3>
               <p>${this._description}</p>
           </div>
        `
        return this._element;
    }

}

class CharacterCard {
    constructor(targetElement, name) {
        this._name = name;
        this._targetElement = targetElement;
        this._element = document.createElement('li');
        this._element.classList.add('character__card');
    }

    render() {
        this._element.innerText = this._name;
        this._targetElement.insertAdjacentElement('beforeend', this._element);
    }
}


document.addEventListener('DOMContentLoaded', () => {
    sendRequest(API_URL, 'GET').then((films) => {
        films.forEach((film) => {
            const filmCard = new FilmCard(film['episodeId'], film['name'], film['openingCrawl']);
            filmsList.insertAdjacentElement('beforeend', filmCard.render());

            const charactersList = document.createElement('ul');
            charactersList.classList.add('characters__list');
            filmCard.element.insertAdjacentElement('beforeend', charactersList);
            film.characters.forEach((character) => {
                sendRequest(character, 'GET').then((character) => {
                    const characterCard = new CharacterCard(charactersList, character['name']);
                    characterCard.render();
                })
            });
        })
    });
});